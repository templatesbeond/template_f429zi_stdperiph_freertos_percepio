/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "printf.h"

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Exported constants --------------------------------------------------------*/
#define LED_NUCLEO144_LED1_PIN 		GPIO_Pin_0
#define LED_NUCLEO144_LED2_PIN 		GPIO_Pin_7
#define LED_NUCLEO144_LED3_PIN 		GPIO_Pin_14
#define LED_NUCLEO144_PORT 			GPIOB

/* Exported macro ------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef enum
{
	eRCC_HSI = (uint8_t)0,
	eRCC_HSE
} eRCC_Oscillator;

typedef enum
{
	eDWT_ms,
	eDWT_us
} eDWT_TimeUnit;

/* Exported functions ------------------------------------------------------- */
/*Configuration Frequency of MCU*/
void vRCC_Init(eRCC_Oscillator eOsc);

/* Data Watchpoint and Trace (DWT) - counts execution cycles */
void vDWT_Init( void );
uint32_t u32DWT_GetTick( void );
void vDWT_Delay( uint32_t u32Time, eDWT_TimeUnit eUnit );
float fDWT_ConvTick( uint32_t u32Tick, eDWT_TimeUnit eUnit );

/*Instrumentation Trace Macrocell (ITM)*/
void vITM_Init( void );

/* Peripherals Functions */
void vGPIO_Init( void );

/*Heap Functions*/
void vHeapInitDumByte( void );
void vCheckHeapStatus( void );

/*Task Functions*/
void vEng_Cyclic_1ms( void *pvParameters );
void vEng_Cyclic_10ms( void *pvParameters );
void vEng_Cyclic_100ms( void *pvParameters );
void vEng_Cyclic_1000ms( void *pvParameters );

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
